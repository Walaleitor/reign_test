# Full Stack Developer Test for Reign 👑

For development this test i used the next technologies:

Server-side Component: 

- node:lastest-version
- express
- Mongodb
- Typescript

Client-side Component: 

- React / Hooks
- Material-UI

Both components are dockerized and can be run from docker-compose, to run the project we need the following commands:

``` cd backend && npm install && tsc && cd .. ```

This is because the tsc command does not work inside the dockerfile.

Next we run the following command:

``` docker-compose up --build ```

To populate the database we must use the following endpoint.

``` POST -> /init ```

To run the tests we use the following command inside the backend directory.

``` yarn coverage  ``` 

 or

 ``` npm run coverage ```


