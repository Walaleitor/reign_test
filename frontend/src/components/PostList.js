import React, { useState, useEffect } from "react";
import { List, makeStyles, Container } from "@material-ui/core";
import PostElement from "../components/PostElement";
import sortArrayWithDate from "../utils/sortArrayWithDate";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 1800,
    margin: "auto",
  },
}));

function PostList() {
  const classes = useStyles();
  const [posts, setPosts] = useState([]);

  const fetchPost = async () => {
    try {
      const data = await fetch("http://localhost:3000/posts", {
        method: "get",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
        }),
      });
      const response = await data.json();
      let posts = response.posts;
      setPosts(sortArrayWithDate(posts));
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchPost();
  }, []);

  return posts.length === 0 ? (
    <Container>
        we have no news for today
    </Container>
  ) : (
    <List component="ul" className={classes.root}>
      {posts.map((post, index) => {
        return (
          <PostElement
            component="li"
            key={index}
            title={post.story_title}
            author={post.author}
            date={post.created_at}
            id={post._id}
            setPosts={setPosts}
            posts={posts}
          />
        );
      })}
    </List>
  );
}

export default PostList;
