import React, {
  Fragment,
  useEffect,
  useState,
  useCallback,
  useRef,
} from "react";
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Divider,
  ListSubheader,
} from "@material-ui/core";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutlined";
import Moment from "react-moment";

function PostElement({ title, date, author, id, setPosts, posts }) {
  const text = title + " - " + author;
  const [isSending, setIsSending] = useState(false);
  const isMounted = useRef(true);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  const softDeletePost = async () => {
    try {
      const response = await fetch(`http://localhost:3000/deleted/${id}`, {
        method: "delete",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded",
        }),
      });
      if (response.status !== 200) return;
      updatePostState();
    } catch (error) {
      console.log(error);
    }
  };

  const updatePostState = () => {
    setPosts(posts.filter((post) => post._id !== id));
  };

  const sendRequest = useCallback(async () => {
    if (isSending) return;

    setIsSending(true);

    await softDeletePost();

    if (isMounted.current) setIsSending(false);
    // eslint-disable-next-line
  }, [isSending]); // update

  return (
    <Fragment>
      <ListItem>
        <ListItemText primary={text} />
        <ListSubheader>
          <Moment format="YYYY/MM/DD" date={date} />
        </ListSubheader>
        <ListItemSecondaryAction>
          <IconButton edge="end" aria-label="comments" onClick={sendRequest}>
            <DeleteOutlinedIcon  />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider></Divider>
    </Fragment>
  );
}

export default PostElement;
