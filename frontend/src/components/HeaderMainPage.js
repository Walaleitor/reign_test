import React from "react";
import { Container, CardHeader } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

const styles = (theme) => ({
  title: {
    color: "white",
  },
  subheader: {
    color: "white",
  },
  root: {
    background: "black",
  },
});

function HeaderMainPage({ classes }) {
  return (
    <Container
      maxWidth="xl"
      className={classes.root}
      color="black"
      elevation={0}
    >
      <CardHeader
        classes={{
          title: classes.title,
          subheader: classes.subheader,
        }}
        title="HN Feed"
        subheader="We <3 hacker news!"
      ></CardHeader>
    </Container>
  );
}

export default withStyles(styles)(HeaderMainPage);
