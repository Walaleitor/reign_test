import React, { Fragment } from "react";
import HeaderMainPage from "../components/HeaderMainPage";

import PostList from "../components/PostList";

function MainPage({ classes }) {
  return (
    <Fragment>
      <HeaderMainPage />
      <PostList />
    </Fragment>
  );
}

export default MainPage;
