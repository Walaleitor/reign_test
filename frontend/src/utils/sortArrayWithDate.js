
const sortArrayWithDate = (posts) => {
    return posts.sort(function(a, b) {
        var dateA = new Date(a.created_at), dateB = new Date(b.created_at);
        return  dateB - dateA ;
    });
}


export default sortArrayWithDate;