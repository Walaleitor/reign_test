import { Request, Response } from "express";
import { IPost } from "./models.posts";
import PostRepository from "./repository.posts";

export default class PostControllers {
  async getPost(req: Request, res: Response) {
    try {
      const posts: IPost[] = await PostRepository.fetchPostEnabledFromDatabase();
      const count: number = await PostRepository.fecthCountEnabledFromDatabase();
      return res.status(200).json({
        ok: true,
        count,
        posts,
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        error,
      });
    }
  }

  async deletePost(req: Request, res: Response) {
    try {
      const id: string = req.params.id;
      console.log(id)
      const post = await PostRepository.deletePost(id);
      if (post === null)
        return res.status(400).json({
          ok: false,
          message: "post not found",
        });
      return res.status(200).json({
        ok: true,
        message: "post deleted",
        post,
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        error,
      });
    }
  }
}
