import axios from "axios";
import PostDB, { IPost } from "./models.posts";
import { Post } from "../api/dto.api";

export default class PostRepository {
  static async fetchPostFromDatabase(): Promise<IPost[]> {
    try {
      return await PostDB.find({});
    } catch (error) {
      throw new Error(error);
    }
  }

  static async fecthCountEnabledFromDatabase(): Promise<number> {
    try {
      return await PostDB.countDocuments({ enabled: true });
    } catch (error) {
      throw new Error(error);
    }
  }

  static async fetchPostEnabledFromDatabase(): Promise<IPost[]> {
    try {
      return await PostDB.find({ enabled: true });
    } catch (error) {
      throw new Error(error);
    }
  }

  static async savePostInDatabase(apiPost: Post) {
    try {
      let post: IPost = new PostDB({
        created_at: apiPost.created_at,
        title: apiPost.title,
        url: apiPost.url,
        author: apiPost.author,
        points: apiPost.points,
        story_text: apiPost.story_text,
        comment_text: apiPost.comment_text,
        num_comments: apiPost.num_comments,
        story_id: apiPost.story_id,
        story_title: apiPost.story_title,
        story_url: apiPost.story_url,
        parent_id: apiPost.parent_id,
        created_at_i: apiPost.created_at_i,
        objectID: apiPost.objectID,
      });
      await post.save();
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  }

  static async deletePost(id: string): Promise<IPost | null> {
    try {
      return await PostDB.findByIdAndUpdate(id, {
        enabled: false,
      });
    } catch (error) {
      throw new Error(error);
    }
  }
}
