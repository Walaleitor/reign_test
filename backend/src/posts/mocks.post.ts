import { Post } from "../api/dto.api";

const postMock: Post = {
    created_at: new Date("2020-06-10T22:49:24.000Z"),
    title: null,
    url: null,
    author: "speeder",
    points: null,
    story_text: null,
    comment_text:
      "I wasted in my current project too many time trying to make GViz behave. It is a tool that has lots of potential but has some insanely obvious flaws, I literally wasted 8 work days just trying to make a small planning document, because Graphviz won&#x27;t cooperate unless you start to pile up attributes one after the other in Byzantine hacks.<p>1. Why the &quot;cluster&quot; word is needed on subgraph names? I am not making my project in English.<p>2. Why I can&#x27;t move a whole subgraph to the &quot;end&quot; or &quot;start&quot; ? GViz attempts to make all graphs compact (regardless your settings) and thus tends to shove subgraphs in &quot;random&quot; places.<p>3. Why nodes sometimes leave their subgraph and end on another one? Even when the end result makes no sense? (My graph ended with several places where &quot;a&quot; and &quot;c&quot; is on one cluster and &quot;b&quot; is in a another, with lots of &quot;U&quot; shaped edges connecting then)<p>The official website has a bunch of broken links.<p>&quot;Weight&quot; attribute often is ignored...<p>And the list goes on.<p>Stack overflow is full of bizarre solutions to Graphviz behavior, for example minimum edge length (that for some reason is in inches despite most of the planet using mm)",
    num_comments: null,
    story_id: 23475225,
    story_title: "Create diagrams with code using Graphviz",
    story_url:
      "https://ncona.com/2020/06/create-diagrams-with-code-using-graphviz/",
    parent_id: 23475225,
    created_at_i: 1591829364,
    objectID: "23483104",
    _tags: ["asa"],
  };


  export {
    postMock
  }