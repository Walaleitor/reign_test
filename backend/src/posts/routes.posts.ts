import { Router } from "express";
import PostControllers from "./controllers.posts";

const postRouter: Router = Router();
const postControllers: PostControllers = new PostControllers();

postRouter.get("/posts", postControllers.getPost);
postRouter.delete("/deleted/:id", postControllers.deletePost);

export default postRouter;
