import MockDatabase from "../database/mock.db";
import request from "supertest";
import express from "express";
import PostRepository from "./repository.posts";
import { IPost } from "./models.posts";
import { Post } from "../api/dto.api";
import {postMock} from "./mocks.post";

const app = express();
const mockDatabase = new MockDatabase();


describe("test repository", () => {
  beforeAll(async () => await mockDatabase.connect());

  afterEach(async () => await mockDatabase.clearDatabase());

  afterAll(async () => await mockDatabase.closeDatabase());

  test("fetch post when database is empty", async () => {
    const posts: IPost[] = await PostRepository.fetchPostFromDatabase();
    expect(posts).toHaveLength(0);
  });

  test("fecth post when database have one element", async () => {
    await PostRepository.savePostInDatabase(postMock);

    const posts: IPost[] = await PostRepository.fetchPostFromDatabase();
    expect(posts).toHaveLength(1);
    expect(posts[0].author).toBe("speeder");
  });

  test("fetch amount of post enabled", async () => {
    await PostRepository.savePostInDatabase(postMock);
    const count: number = await PostRepository.fecthCountEnabledFromDatabase();
    expect(count).toBe(1);
  });

  test("verify soft delete about a post", async () => {
    await PostRepository.savePostInDatabase(postMock);
    const posts: IPost[] = await PostRepository.fetchPostEnabledFromDatabase();
    let count: number = await PostRepository.fecthCountEnabledFromDatabase();
    expect(count).toBe(1);
    const id: string = posts[0]._id;
    await PostRepository.deletePost(id);
    count = await PostRepository.fecthCountEnabledFromDatabase();
    expect(count).toBe(0);
  });

  test("check if fetch enabled brings only enabled", async () => {
    await PostRepository.savePostInDatabase(postMock);
    let mock2: Post = { ...postMock };
    mock2.author = "speeedeeer";
    await PostRepository.savePostInDatabase(mock2);
    const posts: IPost[] = await PostRepository.fetchPostEnabledFromDatabase();
    let count: number = await PostRepository.fecthCountEnabledFromDatabase();
    expect(count).toBe(2);
    const id: string = posts[0]._id;
    await PostRepository.deletePost(id);
    count = await PostRepository.fecthCountEnabledFromDatabase();
    expect(count).toBe(1);
  });
});

describe("test end to end", () => {
  beforeAll(async () => await mockDatabase.connect());

  afterEach(async () => await mockDatabase.clearDatabase());

  afterAll(async () => await mockDatabase.closeDatabase());

  test("/posts endopoit when database is empty", () => {
    request(app)
      .get("/posts")
      .expect(200)
      .expect((res) => {
        expect(res.body.ok).toBeTruthy;
        expect(res.body.count).toBe(0);
        expect(res.body.posts).toHaveLength(0);
      })
      .end();
  });

});
