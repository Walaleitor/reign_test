import mongoose, { Schema, Document } from "mongoose";

export interface IPost extends Document {
  created_at: Date;
  title: string;
  enabled: boolean;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  objectID: string;
}

let postSchema: Schema = new Schema({
  created_at: {
    type: Date,
  },
  title: { type: String },
  enabled: { type: Boolean, default: true },
  url: { type: String },
  author: { type: String },
  points: { type: Number },
  story_text: { type: String },
  comment_text: { type: String },
  num_comments: { type: Number },
  story_id: { type: Number },
  story_title: { type: String },
  story_url: { type: String },
  parent_id: { type: Number },
  created_at_i: { type: Number },
  objectID: { type: String },
});

export default mongoose.model<IPost>("PostDB", postSchema);
