import mongoose from "mongoose";

export default class MongoDatabase {
  private url: string;
  private options: any = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };

  constructor(url: string) {
    this.url = url;
  }

  static init(urlDB: string) {
    console.log(urlDB);
    return new MongoDatabase(urlDB);
  }

  async connect() {
    mongoose.connect(this.url, this.options, (err) => {
      console.log("Database up!");
    });
    let dbStatus = mongoose.connection;
    dbStatus.on(
      "error",
      console.error.bind(console, "Database Connection error")
    );
  }
}
