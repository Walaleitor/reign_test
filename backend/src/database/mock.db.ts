import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

export default class MockDatabase {
  private mongod: any;

  constructor() {
    this.mongod = new MongoMemoryServer();
  }

  async connect() {
    const uri = await this.mongod.getUri();
    
    const mongooseOpts = {
      useNewUrlParser: true,
      autoReconnect: true,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000,
    };

    await mongoose.connect(uri,mongooseOpts);
  }


  async closeDatabase  () {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await this.mongod.stop();
  };

  async clearDatabase() {
    const collections = mongoose.connection.collections;
  
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  };
}

