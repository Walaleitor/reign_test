import cron, { CronJob } from "cron";
import ApiService from "./service.api";

export default class CronFetchPost {
  private cronJob: CronJob;

  private constructor() {
    this.cronJob = this.cronBuilder();
  }

  static init() {
    return new CronFetchPost();
  }

  start() {
    this.cronJob.start;
  }

  private cronBuilder() {
    //Hacemos una llamada cuando se inicializa el cron para ya tener cargada la base de datos.
    // ApiService.savePostInDatabase();
    return new cron.CronJob(
      "0 */1 * * * ",
      () => {
        console.log("Actualizando feed!");
        ApiService.savePostInDatabase();
      },
      null,
      true,
      "America/Santiago"
    );
  }
}
