import MockDatabase from "../database/mock.db";
import request from "supertest";
import express from "express";

import PostDB, { IPost } from "../posts/models.posts";
import ApiService from "./service.api";
import ApiRepository from "./repository.api";
import { Post } from "./dto.api";

const app = express();

const mockDatabase = new MockDatabase();

describe("test all module", () => {
  beforeAll(async () => await mockDatabase.connect());

  afterEach(async () => await mockDatabase.clearDatabase());

  afterAll(async () => await mockDatabase.closeDatabase());

  test("database is empty", async () => {
    const posts: IPost[] = await PostDB.find();
    expect(posts).toBeNull;
  });

  test("init endpoint", async () => {
    request(app)
      .post("/init")
      .expect(200)
      .expect((res) => {
        expect(res.body.ok).toEqual(true);
        expect(res.body.message).toEqual("Database Initialized");
      });
  });

  // test("Api repository", async () => {
  //   const posts: Post[] = await ApiRepository.fetchFromApi();


  // });
});
