import {Router} from 'express';
import ApiControllers from './controllers.api';

const apiRouter: Router = Router();
const apiControllers: ApiControllers = new ApiControllers();

apiRouter.post('/init', apiControllers.initDatabase);


export default apiRouter;