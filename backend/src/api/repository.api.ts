import axios from "axios";
import { Posts, Post } from "./dto.api";

export default class ApiRepository {
  static async fetchFromApi(): Promise<Post[]> {
    const posts: Posts = await axios
      .get(String(process.env.URL_API))
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.log(error);
      });
    return posts.hits;
  }
}
