import { Post } from "./dto.api";
import ApiRepository from "./repository.api";
import { IPost } from "../posts/models.posts";
import PostRepository from "../posts/repository.posts";

export default class ApiService {
  static async savePostInDatabase() {
    try {
      const apiPosts: Post[] = await ApiRepository.fetchFromApi();
      const dbPosts: IPost[] = await PostRepository.fetchPostFromDatabase();
      for (let i in apiPosts) {
        let result = dbPosts.find(
          (post) => post.objectID === apiPosts[i].objectID
        );
        if (!result) {
          PostRepository.savePostInDatabase(apiPosts[i]);
        }
      }
    } catch (error) {
      throw new Error(error);
    }
  }
}
