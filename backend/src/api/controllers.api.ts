import { Response, Request, response } from "express";
import ApiService from "./service.api";

export default class ApiControllers {
  async initDatabase(req: Request, res: Response) {
    try {
      await ApiService.savePostInDatabase();
      return res.status(200).json({
          ok: true,
          message: "Database Initialized"
      })
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: "Error in Server"
        })
    }
  }
}
