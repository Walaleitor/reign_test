import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
import postRouter from "../posts/routes.posts";
import apiRouter from "../api/routes.api";

export default class Server {
  private app: express.Application;
  private port: number;

  private constructor(port: number) {
    this.app = express();
    this.port = port;
  }

  static init(port: number = 3000) {
    return new Server(port);
  }
  
  start() {
    this.serverConfiguration();
    this.setUpRoutes();
    this.app.listen(this.port, () => {
      console.log(`Server Up in port ${this.port}!!`);
    });
  }


  private serverConfiguration() {
    this.app.use(cors());
    this.app.use(morgan("dev"));
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
  }

  private setUpRoutes() {
    this.app.use(postRouter);
    this.app.use(apiRouter);
  }
}
