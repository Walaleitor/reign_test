import Server from "./server/server";
import MongoDatabase from "./database/mongo.db";
import CronFetchPost from "./api/cron.api";

const server = Server.init(Number(process.env.SERVER_PORT));
const database = MongoDatabase.init(String(process.env.MONGO_URL));
const cronJob = CronFetchPost.init();

server.start();
database.connect();
cronJob.start();
